import pytest

from multiranges.multiranges import MultiRange, argToRange
from multiranges.exceptions import NoRangeableObjectError


@pytest.mark.parametrize('arg, _range', [
    (1, range(1)),
    ((5, 9), range(5, 9)),
    (['1', 2, '1234'], ['1', 2, '1234']),
    (range(41), range(41)),
    ((1, 2, 3, 4), [1, 2, 3, 4]),
    ({1, 2, 3}, [1, 2, 3])
])
def testArgToRange(arg, _range):
    assert argToRange(arg) == _range


def testArgToRangeError():
    with pytest.raises(NoRangeableObjectError):
        argToRange(object())


def testSingleIntMRGivesListOfInts():
    mr = MultiRange(5)
    assert list(mr) == [0, 1, 2, 3, 4]


def testStringMRGivesListOfChars():
    mr = MultiRange('iamatest')
    assert list(mr) == ['i', 'a', 'm', 'a', 't', 'e', 's', 't']


def testEmptyMRGiveEmptyList():
    mr = MultiRange()
    assert list(mr) == []


@pytest.mark.parametrize('ranges, lenght', [
    ([1, 5], 5),
    ([115], 115),
    ([['+', '-'], range(5, 9), 10], 2 * 4 * 10),
    ([], 0)
])
def testLenghts(ranges, lenght):
    mr = MultiRange(*ranges)
    assert len(mr) == lenght


def testMRreturns():
    mr = MultiRange(['+', '-'], range(2), 'e', range(3))
    expecs = [
        ('+', 0, 'e', 0), ('+', 0, 'e', 1), ('+', 0, 'e', 2),
        ('+', 1, 'e', 0), ('+', 1, 'e', 1), ('+', 1, 'e', 2),
        ('-', 0, 'e', 0), ('-', 0, 'e', 1), ('-', 0, 'e', 2),
        ('-', 1, 'e', 0), ('-', 1, 'e', 1), ('-', 1, 'e', 2),
    ]
    for i, t in enumerate(mr):
        assert expecs[i] == t


def testHighIndexRaisesIndexError():
    mr = MultiRange(2, 3)
    with pytest.raises(IndexError):
        _ = mr[144]


def testIndexes():
    mr = MultiRange(12, 21)
    assert mr[0] == (0, 0)
    assert mr[21] == (1, 0)
    assert mr[len(mr) - 1] == (11, 20)


def testNestedMR():
    mr1 = MultiRange(['+', '-'], 3)
    mr2 = MultiRange(5, mr1)
    assert mr1[0] == ('+', 0)
    assert list(mr2)[0] == (0, ('+', 0))


def testIndexesWithNestedMR():
    mr = MultiRange(3, MultiRange('hello', range(2, 4)), 'world')
    assert mr[0] == (0, ('h', 2), 'w')
    assert mr[4] == (0, ('h', 2), 'd')
    assert mr[5] == (0, ('h', 3), 'w')
    assert mr[9] == (0, ('h', 3), 'd')
    assert mr[10] == (0, ('e', 2), 'w')
